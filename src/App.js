import React from 'react';

import './App.css';
import { Layout, Menu } from 'antd';

import 'antd/dist/antd.css';
import { List, Card } from 'antd';
function App() {
const { Header, Footer } = Layout;
const data = [
  {
    title: 'Advanced illustrator',
    Details: 'คอร์สการสอน illustrator เพื่อต่อยอดด้านธุรกิจ สำหรับผู้ที่มีพื้นฐานการทำงานด้านออกแบบแต่ต้องการคำแนะนำ และเทคนิคในการใช้โปรแกรมที่ช่วยให้การทำงานของคุณง่ายนี้ การเรียนการสอนเน้นเข้าใจงาน'
  },
  {
    title: 'Basic Illustrator',
    Details: 'illustrator เป็นโปรแกรมที่ได้รับความนิยมเป็นอย่างมากในด้านการออกแบบ โดยผู้เรียนสามารถต่อยอดเพื่อสร้างรายได้จากการออกแบบได้มากมาย โดยหลักสูตรการสอนนี้จะเน้นพื้นฐานสำหรับใช้ตัวโปรแกรม'
  },
  {
    title: 'Basic Photoshop',
    Details: 'Photoshop เป็นโปรแกรมสร้างและแก้ไขรูปภาพอย่างมืออาชีพโดยเฉพาะนักออกแบบในทุกวงการย่อมรู้จักโปรแกรมตัวนี้ดี โดยในหลักสูตรนี้จะเน้นการรู้จักเครื่องมือต่างๆ การไดคัทตกแต่งภาพ'
  },
  {
    title: 'Basic After Effect',
    Details: 'หากคุณคือคนสนใจด้านการทำ แอนนิเมชั่นแต่ยังไม่มีพื้นฐาน คอร์สนี้คือทางเลือกสำหรับคุณ เพราะเราเน้นการสอนสำหรับผู้ที่ยังไม่มีประสบการณ์ โดยการเรียนการสอนจะสอนการใช้โปรแกรม After Effect เบื้องต้น'
  },
];
//   const getGreeting = () =>{
// return 'Welcome to React';
//   }
//   const Welcome=(props) =>{
//   return <h1>Hello, {props.name}</h1>
//   }

  return (
    // <div className="App">
    //   <header className="App-header">
    //   </header>
    //   <h2>{getGreeting()}</h2>
    // <h1><Welcome name="Cake"/></h1>
    //  <button>Click</button>
    // </div>
    
    <Layout className="layout">
    <Header style={{ background: 'rgb(37, 101, 167)'}}>
     
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['']}
        style={{ lineHeight: '63px', background: 'rgb(37, 101, 167)'}}
      >
        <Menu.Item key="1" style={{color: 'rgb(255, 255, 255)'}}>คอร์สเรียน</Menu.Item>
        <Menu.Item key="2" style={{color: 'rgb(255, 255, 255)'}}>วิธีซื้อคอร์สเรียน</Menu.Item>
        <Menu.Item key="3" style={{color: 'rgb(255, 255, 255)'}}>รีวิวคอร์สเรียน</Menu.Item>
      </Menu>
    </Header>
   <p></p>
   <List
    grid={{ gutter: 16, column: 4 }}
    dataSource={data}
    renderItem={item => (
      <List.Item>
        <Card title={item.title}>{item.Details}</Card>
        
      </List.Item>
    )}
  />
    <Footer style={{ textAlign: 'center' ,lineHeight: '63px', background: 'rgb(37, 101, 167)',color: 'rgb(255, 255, 255)'}}> ©2020 Created by CAKE</Footer>
  </Layout>
 
  
  );
}

export default App;
